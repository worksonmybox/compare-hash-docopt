# Compare Hash

## What this is

This is a command line tool that allows a user to provide two directory paths in order to hash the files in each directory to ensure the files are the same. It will also alert the user of any files which are not shared between the directories. This tool uses a flag for large files and if it is turned on, can churn through some really large files fairly quickly. On my 2700X, I was able to get through ~40GB of files in between 11-15 seconds. On a single thread, this would take much much longer.

## Requirements

Python 3.7+

pip package <code>docopt</code>

## Usage
compare.py debug

compare.py -h | --help

compare.py --version

compare.py compare-hashes SOURCE DESTINATION [--hash HASH] [-r | --recursive]
[-l | --large] [-v] [--vv] [debug]

## Options
-h, --help                   Show this screen

--version                    Show version

-l, --large                  A flag to let the program know whether the files are large files (this can greatly increase/reduce the time it takes to hash files), default False

--hash HASH                  Hash algorithm to use for file hashes (sha1, sha224, sha256, sha 384, sha512, pbkdf2_hmac), default sha256

-r, --recursive                  A flag to set whether operations should be recursive, default False

-v                           A flag to set verbosity to gather more information/logging, default False

--vv                         A flag to set double verbosity to gather more information/logging, default False