"""Compare-Hashes

Usage:
  compare.py debug
  compare.py -h | --help
  compare.py --version
  compare.py compare-hashes SOURCE DESTINATION [--hash HASH] [-t | --time] [-r | --recursive] [-l | --large] [-v] [--vv] [debug]

Options:
  -h, --help                   Show this screen
  --version                    Show version
  -l, --large                  A flag to let the program know whether the files are large files (this can greatly increase/reduce the time it takes to hash files), default False
  --hash HASH                  Hash algorithm to use for file hashes (sha1, sha224, sha256, sha 384, sha512, pbkdf2_hmac), default sha256
  -r, --recursive              A flag to set whether operations should be recursive, default False
  -t, --time                   A flag to print the time it took to complete hashing comparison operation, default False
  -v                           A flag to set verbosity to gather more information/logging, default False
  --vv                         A flag to set double verbosity to gather more information/logging, default False

"""
from docopt import docopt
from pathlib import Path
from pprint import pprint
import hashlib
import concurrent.futures
import timeit


def main():
    arguments = docopt(__doc__, version="Compare-Hashes 0.1")
    if arguments.get("debug"):
        print(arguments)
    if arguments.get("compare-hashes"):
        compare_hashes(
            arguments.get("SOURCE"),
            arguments.get("DESTINATION"),
            arguments.get("--hash"),
            arguments.get("--time"),
            arguments.get("--recursive"),
            arguments.get("--large"),
            arguments.get("-v"),
            arguments.get("--vv"),
        )


def get_colors():
    return {
        "black": "\u001b[30m",
        "red": "\u001b[31m",
        "green": "\u001b[32m",
        "yellow": "\u001b[33m",
        "blue": "\u001b[34m",
        "magenta": "\u001b[35m",
        "cyan": "\u001b[36m",
        "white": "\u001b[37m",
        "reset": "\u001b[0m",
    }


def process_hash(source_file: Path, hash_: str) -> dict:
    BLOCK_SIZE = 2 ** 20
    source_hash = get_hash(hash_)
    source_dict = {}

    if not source_file.is_dir():
        with open(source_file, "rb") as source:
            for chunk in iter(lambda: source.read(BLOCK_SIZE), b""):
                source_hash.update(chunk)
            source_dict.update({source_hash.hexdigest(): source_file.parts[-1]})

    return source_dict


def get_hash(hash_: str) -> str:
    hash_ = hash_.lower()
    if hash_ == "md5":
        return hashlib.md5()
    elif hash_ == "sha1":
        return hashlib.sha1()
    elif hash_ == "sha224":
        return hashlib.sha224()
    elif hash_ == "sha256":
        return hashlib.sha256()
    elif hash_ == "sha384":
        return hashlib.sha384()
    elif hash_ == "sha512":
        return hashlib.sha512()
    elif hash_ == "pbkdf2_hmac":
        return hashlib.pbkdf2_hmac()
    else:
        print(
            "Invalid hash provided\nOptions: \n  md5\n  sha1\n  sha224\n  sha256\n  sha384\n  sha512\n  pbkdf2_hmac"
        )
        raise ValueError


def compare_hashes(
    source_path: str,
    destination_path: str,
    hash_,
    time_: bool = False,
    recursive: bool = False,
    large: bool = False,
    v: bool = False,
    vv: bool = False,
) -> None:
    start_time = timeit.default_timer()

    if not hash_:
        hash_ = "sha256"

    colors = get_colors()
    source_path = Path(source_path).resolve()
    destination_path = Path(destination_path).resolve()

    if not source_path.exists():
        print("Source path does not exist")
        return

    if not destination_path.exists():
        print("Destination path does not exist")
        return

    if source_path == destination_path:
        print(
            f"{colors['yellow']}You provided the same directory for both source and destination...{colors['reset']}"
        )

    source_dict = {}
    destination_dict = {}

    if not large and not recursive:
        results = [process_hash(item, hash_) for item in source_path.glob("*")]
        for result in results:
            source_dict.update(result)

        results = [process_hash(item, hash_) for item in destination_path.glob("*")]
        for result in results:
            destination_dict.update(result)

    elif not large and recursive:
        results = [process_hash(item, hash_) for item in source_path.rglob("*")]
        for result in results:
            source_dict.update(result)

        results = [process_hash(item, hash_) for item in destination_path.rglob("*")]
        for result in results:
            destination_dict.update(result)

    elif large and not recursive:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = []
            for item in source_path.glob("*"):
                futures.append(
                    executor.submit(process_hash, source_file=item, hash_=hash_)
                )
            for future in concurrent.futures.as_completed(futures):
                source_dict.update(future.result())

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = []
            for item in destination_path.glob("*"):
                futures.append(
                    executor.submit(process_hash, source_file=item, hash_=hash_)
                )
            for future in concurrent.futures.as_completed(futures):
                destination_dict.update(future.result())

    elif large and recursive:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = []
            for item in source_path.rglob("*"):
                futures.append(
                    executor.submit(process_hash, source_file=item, hash_=hash_)
                )
            for future in concurrent.futures.as_completed(futures):
                source_dict.update(future.result())

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = []
            for item in destination_path.rglob("*"):
                futures.append(
                    executor.submit(process_hash, source_file=item, hash_=hash_)
                )
            for future in concurrent.futures.as_completed(futures):
                destination_dict.update(future.result())

    print(
        f"Source files: {len(source_dict)}\nDestination files: {len(destination_dict)}"
    )

    errors = 0
    source_missing = 0
    destination_missing = 0
    shared_files = False

    for key, value in source_dict.items():
        if value in destination_dict.values() and key in destination_dict:
            shared_files = True
        elif value in destination_dict.values() and key not in destination_dict:
            errors += 1
            if v or vv:
                print(
                    f"{colors['red']}Hash for file {value} does not match.{colors['reset']}"
                )
        else:
            source_missing += 1
            if vv:
                print(f"{colors['yellow']}{value} not in destination{colors['reset']}")

    for key, value in destination_dict.items():
        if value not in source_dict.values():
            destination_missing += 1
            if vv:
                print(f"{colors['yellow']}{value} not in source{colors['reset']}")

    if not errors and shared_files:
        if source_missing:
            print(
                f"{colors['yellow']}There are {source_missing} files that exist in source that do not exist in destination.{colors['reset']}"
            )
        if destination_missing:
            print(
                f"{colors['yellow']}There are {destination_missing} files that exist in destination that do not exist in source.{colors['reset']}"
            )

        print(
            f"{colors['green']}All hashes match for files that exist in both source and destination directories.{colors['reset']}"
        )

    elif not errors and not shared_files:
        print(
            f"{colors['yellow']}Source directory shares zero files with destination directory.\nNote: If the source/destination directories only contain directories and the --recursive flag was not set, try setting the --recursive flag.{colors['reset']}"
        )

    elif errors:
        print(
            f"{colors['red']}There are {errors} errors.{colors['reset']}"
        ) if errors > 1 else print(
            f"{colors['red']}There is {errors} error.{colors['reset']}"
        )

    if time_:
        print(
            f"Hashing operation completed in {timeit.default_timer() - start_time:.2f}s"
        )


if __name__ == "__main__":
    main()
